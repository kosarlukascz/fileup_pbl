<?php

if(!function_exists('redir')) {
    function redir($href) {
        header('Location: ' . $href);
        exit;
    }
}

if(!function_exists('post')) {
    function post($key) {
        return isset($_POST[$key]) ? $_POST[$key] : false;
    }
}

if(!function_exists('get')) {
    function get($key) {
        return isset($_GET[$key]) ? $_GET[$key] : false;
    }
}

if(!function_exists('fp')) {
    function fp($key, $value)
    {
        if(post($key)) {
            return post($key) == $value ? true : false;
        }
        return false;
    }
}

if(!function_exists('fg')) {
    function fg($key, $value)
    {
        if(get($key)) {
            return get($key) == $value ? true : false;
        }
        return false;
    }
}