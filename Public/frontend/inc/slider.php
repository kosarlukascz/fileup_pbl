<div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item active" style="background-image: url(images/slider/1.jpg)">
            <div class="caption">
                <h1 class="animated fadeInLeftBig">Vítejte na <span>FileUP!</span></h1>
                <p class="animated fadeInRightBig">Uložiště obrázků - Moderní design - Neomezený prostor - Každým dnem něco nového!</p>
                <p class="animated fadeInRightBig">PS: Zatím jen beta...</p>
                <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Zjisti více!</a>
            </div>
        </div>
        <div class="item" style="background-image: url(images/slider/2.jpg)">
            <div class="caption">
                <h1 class="animated fadeInLeftBig">Očekávej nové možnosti! <span>FileUP.CZ</span></h1>
                <p class="animated fadeInRightBig">Uložiště obrázků - Moderní design - Neomezený prostor - Každým dnem něco nového! </p>
                <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Zjisti více!</a>
            </div>
        </div>
        <div class="item" style="background-image: url(images/slider/3.jpg)">
            <div class="caption">
                <h1 class="animated fadeInLeftBig">My jsme <span>FileUp.CZ</span> !</h1>
                <p class="animated fadeInRightBig">Uložiště obrázků - Moderní design - Neomezený prostor - Každým dnem něco nového!</p>
                <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Zjisti víc!</a>
            </div>
        </div>
    </div>
    <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
    <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

    <a id="tohash" href="#services"><i class="fa fa-angle-down"></i></a>

</div>