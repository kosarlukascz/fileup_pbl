<?php
include 'inc/inc.php'
?>
<?php
// konfigurace
$uploadDir = './upload'; // adresar, kam se maji nahrat obrazky (bez lomitka na konci)
$uploadDirThumb = './upload/thumbs';
$allowedExt = array('jpg', 'jpeg', 'png', 'gif'); // pole s povolenymi priponami
$results = array();

// zpracovani uploadu
if(isset($_FILES['obrazky']) && is_array($_FILES['obrazky']['name'])) {

    $counter = 0;
    $allowedExt = array_flip($allowedExt);
    foreach($_FILES['obrazky']['name'] as $klic => $nazev) {

        $fileName = basename($nazev);
        $tmpName = $_FILES['obrazky']['tmp_name'][$klic];
        $size = $_FILES['obrazky']['size'][$klic];

        // kontrola souboru
        if(
            !is_uploaded_file($tmpName)
            || !isset($allowedExt[strtolower(pathinfo($fileName, PATHINFO_EXTENSION))])
        ) {
            // neplatny soubor nebo pripona
            continue;
        }

        $pap = $uploadDir.DIRECTORY_SEPARATOR.time()."-".$fileName;
        $pap2 = $uploadDirThumb.DIRECTORY_SEPARATOR.time()."-".$fileName;
        $pripona = pathinfo($fileName);

        // presun souboru
        if(move_uploaded_file($tmpName, $pap)) {
            ++$counter;


            ($uploadDir.DIRECTORY_SEPARATOR.time()."-".$fileName);

            $img = new abeautifulsite\SimpleImage($pap);
            $img->best_fit(220,220);
            $img->save($pap2);


            $results[] = "<a target='_blank' href='http://user.fileup.cz/upload/".time()."-".$fileName."'><button id='result' class='btn-flat btn-block'><b><p>".$counter." z ".sizeof($_FILES['obrazky']['name'])." souborů.</p></b></button></a>";
            db::insert('soubory',array('user_id' => $_SESSION['uzivatel_id'], 'img_link' => $pap,'img_link_thumb' => $pap2,'pripona' => $pripona['extension'],'velikost' => $size));
            $nahrano = '1';

        }
    }
}
?>
<?
$uzivatel_mist = dibi::fetchSingle('select uplimit from uzivatele where uzivatele_id = ' . $_SESSION['uzivatel_id']);
$zaplneno = dibi::fetchSingle('select count(*) from soubory where user_id = ' . $_SESSION['uzivatel_id']);
$procento = ($uzivatel_mist/100);
$procento_uziv = ($zaplneno/$procento);
$vysledek_procent = round($procento_uziv,0)
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>FileUP | Nahrávání </title>
    <? include'inc/head.php'; ?>
    <style>
        .cudlik {
            width: 100%;
            height: 140px;
            color: white;
            font-weight: bold;
            border: 2px solid #027db3;
            border-radius: 5px;
            background-color: #2A3F54;
        }
        .cudlik:hover {
            width: 100%;
            height: 140px;
            color: white;
            font-weight: bold;
            border: solid 2px #2A3F54;
            border-radius: 5px;
            background-color: #027db3;
        }
        #result {
            width: 100%;
            height: 30px;
            color: white;
            font-weight: bold;
            background-color: #2A3F54;
        }
        #result:hover {
            width: 100%;
            height: 30px;
            color: white;
            font-weight: bold;
            background-color: #027db3;
        }
        label.myLabel input[type="file"] {
            position: fixed;
            top: -1000px;
        }

        .myLabel {
            border: 2px solid #027db3;
            border-radius: 4px;
            height: 140px;
            width: 100%;
            background: #172d44;
            display: inline-block;
        }
        }
        .myLabel:hover {
            border: 2px solid #172d44;
            border-radius: 4px;
            height: 140px;
            width: 100%;
            background: #027db3;
            display: inline-block;
        }
        .myLabel:active {
            background: #ffffff;
        }
        .myLabel :invalid + span {
            color: #ffffff;
        }
        .myLabel :valid + span {
            color: #00f3ff;
        }
        #warn {
            color: black;
        }
    </style>
    <? include'inc/right.php'; ?>
    <? include'inc/header.php'; ?>
    <!--  content -->
    <div class="right_col" role="main">

        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Nahrávání souborů</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <? if($nahrano == '1') {?>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>Nahrané soubory</h2>

                            <div class="clearfix"><p></p></div>
                        </div>
                            <div class="col-sm-12 col-xs-12 col-md-6 center">
                            <? foreach($results as $result) {echo $result; }?>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-6 center">
                         <div class="col-xs-6"><a href="gallery.php" style="background-color:#172d44;color: white; " class="btn btn-default btn-block">Přejít do galerie</a></div>
                            <div class="col-xs-6"><a href="upload.php" style="background-color:#027db3;color: white; " class="btn btn-default btn-block">Nahrát další soubory</a></div>
                        </div>
                </div>
                </div>
    <? } else { ?><div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>Nahrávání souborů</h2>

                            <div class="clearfix"><p></p></div>
                        </div>
                        <form method="post" id="form" enctype="multipart/form-data">
                        <div class="col-sm-3 col-xs-3 center">
                            <span id="submitContent"><input type="submit" id="submitButton"  class="btn cudlik" value="Nahrát"></span>
                        </div>
                            <div class="col-sm-9 col-xs-9 center">
                                <label class="myLabel">
                                    <input type="file" name="obrazky[]" class="btn" id="nahraj" required="required" multiple="multiple">
                                    <span style="position: absolute;margin-left: 42%;margin-top: 60px;">Vybrat&nbsp;soubory</span>
                                </label>
                        </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
                        <div class="x_panel" style="">
                            <div class="x_title">
                                <h2>Nahrávání souborů</h2>

                                <div class="clearfix"><p></p></div>
                            </div>
                            <form method="post" id="form" enctype="multipart/form-data">
                                <div class="col-sm-12 col-xs-12 center">
                                    <span id="submitContent"><input type="submit" id="submitButton" style="color: white; background-color:#172d44; " class="btn btn-default btn-block" value="Nahrát"></span>
                                </div>
                                <div class="col-sm-12 col-xs-12 center">
                                    <input type="file" name="obrazky[]" class="btn btn-default btn-block" style="color: white; background-color:#027db3;" id="nahraj" required="required" multiple="multiple">
                                </div>
                            </form>
                        </div>
                    </div>
                <?}?>
        </div></div>
        <div class="row">
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="x_panel" style="">
                <div class="x_title">
                    <h2>Statistiky</h2>
                    <div class="clearfix"><p></p></div>
                </div>
                <script type="text/javascript">
                    $(function() {
                        //create instance
                        $('.chart').easyPieChart({
                            animate: 2000
                        });
                    });
                </script>
                <div class="col-sm-12 col-xs-12 center">
                    <div class="col-xs-8 col-sm-8"><label id="warn">
                    <center style="margin-top: 12%;">Nyní máte zaplněno <b><?echo $zaplneno;?></b> souborů z celkového počtu <b><?echo $uzivatel_mist;?></b> souborů.</center>
                     </div>
                    <div class="col-xs-4 col-sm-4">
                            <span class="chart img-responsive" data-percent="<?echo $vysledek_procent;?>">
                                            <span class="percent"><?echo $vysledek_procent;?></span>
                            </span>
                    </div></label>
                </div>
            </div>
        </div>
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="x_panel" style="">
                        <div class="x_title">
                            <h2>Informace</h2>
                            <div class="clearfix"><p></p></div>
                        </div>
                        <div class="col-sm-12 col-xs-12 center">
                            <div class="alert alert-info" style="font-weight: bold;" role="alert">
                                Pokud máte nedostatek místa, nemusíte zoufat, snadno můžete <a class="alert-link" href="#">získat další</a>. Stačí nás <a class="alert-link" href="#">kontaktovat</a> a domluvit nějaké řešení. Nejsnadnější cesta k více místu je sdílení FileUP! mezi své přátelé.
                        </div>
                        </div>
                    </div>
            </div>
        </div>
        <? include'inc/footer.php'; ?>
    </div>
    <!-- / content -->
    <script>
        $('#submitButton').on('click',function() {
            $('#form').submit();
            $('#submitContent').html('<div style="margin-top: 60px;"><i style="color: #172d44;" class="fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom"></i> <b style=" font-weight: bold;">Nahrávání</b></div>');
        });
    </script>
<? include'inc/bottom.php';?>