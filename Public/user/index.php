<?php include'inc/inc.php';?>
<?php
$uzivatele = db::querySingle('SELECT COUNT(*) FROM uzivatele');
$soubory = db::querySingle('SELECT COUNT(*) FROM soubory');
$soubory_uziv = db::querySingle('select count(*) from soubory where user_id = ' . $_SESSION['uzivatel_id']);
$celkovavelikost = db::querySingle('select sum(velikost) from soubory');
$uzivatelovavelikost = db::querySingle('select sum(velikost) from soubory where user_id = ' . $_SESSION['uzivatel_id']);
?>
<?
if ($soubory_uziv<1) { //nahral uzivatel alespon jeden soubor - nevypise NAN
    $soubory_uzivatel = '0';
}else {
    $soubory_uzivatel = $soubory_uziv;
}

if ($soubory<1) { //Neni server prazdny - nevypise NAN
    $soubory_celkem = '0';
}else {
    $soubory_celkem = $soubory;
}
?>
<?php
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', ' KB', ' MB', ' GB', ' TB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>FileUP | Rozhraní </title>
<? include'inc/head.php'; ?>
<? include'inc/right.php'; ?>
<? include'inc/header.php'; ?>
    <!--  content -->
    <div class="right_col" role="main">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-file"></i>
                    </div>
                    <div class="count"><? echo $soubory_celkem; ?></div>
                    <br/>
                    <h3>Celkově nahraných souborů</h3>
                    <p>Počet souborů, které nahráli všichni uživatelé.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-file-o"></i>
                    </div>
                    <div class="count"><? echo $soubory_uzivatel; ?></div>
                         <br/>
                    <h3>Vámi nahraných souborů</h3>
                    <p>Počet souborů, které jste nahráli na FileUP! Vy.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-folder-open-o"></i>
                    </div>
                    <div class="count"><?=formatBytes($uzivatelovavelikost,0) ?></div>
                        <br/>
                    <h3>Místo, které je zaplněné Vámi</h3>
                    <p>Místo, které jste zaplnily na FilUP! Vy.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-folder-open"></i>
                    </div>
                    <div class="count"><?=formatBytes($celkovavelikost,0);?></div>
                           <br/>
                    <h3>Celkově zaplněné místo</h3>
                    <p>Místo, které využívají všichni uživatelé FileUP!</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">

                    <div class="row x_title">
                        <div class="col-md-6">
                            <h3>Empty <small>no content</small></h3>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        ...
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <br />

        <script src="js/pace/pace.min.js"></script>

        <? include'inc/footer.php'; ?>
    </div>
    <!-- / content -->
<? include'inc/bottom.php';?>